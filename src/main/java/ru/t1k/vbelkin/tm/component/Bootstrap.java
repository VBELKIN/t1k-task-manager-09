package ru.t1k.vbelkin.tm.component;

import ru.t1k.vbelkin.tm.api.ICommandController;
import ru.t1k.vbelkin.tm.api.ICommandRepository;
import ru.t1k.vbelkin.tm.api.ICommandService;
import ru.t1k.vbelkin.tm.constant.ArgumentConst;
import ru.t1k.vbelkin.tm.constant.TerminalConst;
import ru.t1k.vbelkin.tm.controller.CommandController;
import ru.t1k.vbelkin.tm.repository.CommandRepository;
import ru.t1k.vbelkin.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                showErrorCommand();
        }
    }

    private void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                showErrorArgument();
        }
    }

    private void exit() {
        System.exit(0);
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void run(final String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }
        processCommands();
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public void showErrorCommand() {
        System.err.println("Error! This command not supported...");
    }

    public void showErrorArgument() {
        System.err.println("Error! This argument not supported...");
    }

}
